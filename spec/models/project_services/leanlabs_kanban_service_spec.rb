# == Schema Information
#
# Table name: services
#
#  id                    :integer          not null, primary key
#  type                  :string(255)
#  title                 :string(255)
#  project_id            :integer
#  created_at            :datetime
#  updated_at            :datetime
#  active                :boolean          default(FALSE), not null
#  properties            :text
#  template              :boolean          default(FALSE)
#  push_events           :boolean          default(TRUE)
#  issues_events         :boolean          default(TRUE)
#  merge_requests_events :boolean          default(TRUE)
#  tag_push_events       :boolean          default(TRUE)
#  note_events           :boolean          default(TRUE), not null
#

require 'spec_helper'

describe LeanlabsKanbanService do
  include LeanlabsKanbanHelper
  describe "Associations" do
    it { should belong_to :project }
  end

  describe "Validations" do
    context "active" do
      before do
        subject.active = true
      end

      it { should validate_presence_of :leanlabs_kanban_url }
    end
  end

  describe 'Leanlabs Kanban' do
    let(:project) { create(:project) }

    context 'when it is active' do
      before do
        properties = { 'leanlabs_kanban_url' => 'https://kanban.gitlab.com' }
        @service = project.create_leanlabs_kanban_service(active: true, properties: properties)
      end

      after do
        @service.destroy!
      end

      it 'should generate correct board url' do
        board_path = get_project_board_path(project)
        expect(board_path).to eq("https://kanban.gitlab.com/boards/#{project.path_with_namespace}")
      end
    end
  end
end
