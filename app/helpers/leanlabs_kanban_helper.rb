module LeanlabsKanbanHelper
  def get_project_board_path(project)
    "#{project.leanlabs_kanban_service.properties['leanlabs_kanban_url']}/boards/#{project.path_with_namespace}"
  end
end
