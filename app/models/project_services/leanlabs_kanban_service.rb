# == Schema Information
#
# Table name: services
#
#  id                    :integer          not null, primary key
#  type                  :string(255)
#  title                 :string(255)
#  project_id            :integer
#  created_at            :datetime
#  updated_at            :datetime
#  active                :boolean          default(FALSE), not null
#  properties            :text
#  template              :boolean          default(FALSE)
#  push_events           :boolean          default(TRUE)
#  issues_events         :boolean          default(TRUE)
#  merge_requests_events :boolean          default(TRUE)
#  tag_push_events       :boolean          default(TRUE)
#  note_events           :boolean          default(TRUE), not null
#

class LeanlabsKanbanService < Service
  include HTTParty

  prop_accessor :leanlabs_kanban_url
  validates :leanlabs_kanban_url,
            presence: true,
            format: { with: /\A#{URI.regexp}\z/ },
            if: :activated?

  def title
    'Leanlabs Kanban'
  end

  def description
    'Enables Leanlabs Kanban integration'
  end

  def to_param
    'leanlabs_kanban'
  end

  def supported_events
    %w()
  end

  def fields
    [
      { type: 'text', name: 'leanlabs_kanban_url', placeholder: 'The URL of the Leanlabs Kanban' },
    ]
  end

  def execute(_data)
    @response = HTTParty.get(properties['leanlabs_kanban_url'], verify: true) rescue nil
    if !@response.nil? && @response.code == 200
      true
    end
  end
end
